\documentclass{acm_proc_article-sp}
\usepackage[utf8x]{inputenc}
\usepackage{hyperref}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{balance}

\begin{document}

\title{A GPGPU Implementation of CMA-ES}

\numberofauthors{2} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Aleksandar Tošić\\
       \affaddr{Faculty of Mathematics, Natural Sciences and Information Technologies }\\
       \affaddr{University of Primorska}\\
       \email{aleksandar.tosic@upr.si}
\alignauthor
Matjaž Šuber\\
       \affaddr{Faculty of Mathematics, Natural Sciences and Information Technologies }\\
       \affaddr{University of Primorska}\\
       \email{matjaz.suber@student.upr.si}
}
% There's nothing stopping you putting the seventh, eighth, etc.
% author on the opening page (as the 'third row') but we ask,
% for aesthetic reasons that you place these 'additional authors'
% in the \additional authors block, viz.

\date{}

\maketitle

\begin{abstract}
In the last decade, a lot of hope has been put into General Purpose computing on Graphical Processing Units with optimization algorithms being among the most researched \cite{pharr2005gpu, harris2005gpgpu, tsutsui2013massively, wen2011gpu}. Unfortunately, the shift in programming paradigms and computation models makes implementation of existing algorithms non-trivial. Some algorithms are simply not suitable for implementation on a single Instruction Multiple Data (SIMD) model.
To overcome the tediousness of programming GPU's a number of frameworks became available with Compute Unified Device Architecture (CUDA) \cite{nvidia2008programming} being the most popular proprietary and Open Computing Language (OpenCL) \cite{munshi2009opencl} leading in the field of open-source frameworks.
With the help of OpenCL we implemented the Covariance Matrix Adaptation Evolution Strategy (CMA-ES) \cite{hansen2006cma} in a Metaheuristic Algorithms in Java (jMetal) framework \cite{DNA10, Durillo2011}.
We tested both CPU and OpenCL implementations on different hardware configurations and problem sets available in jMetal. Additionally we explain the details of our implementation and show a comparison of computation results.
\end{abstract}

\keywords{Covariance Matrix Adaptation Evolution Strategy(CMA), Numerical Optimisation, Evolutionary Algorithm, GPGPU, jMetal, OpenCL} 

\section{Introduction}
Computation on graphics processing units has received a lot of attention in the past. Mainly because of the availability of hardware that was driven mostly by the gaming industry \cite{macedonia2003gpu}. As more complex rendering engines were developed, the need to harness the GPU capabilities grew. Manufacturers gave more access and control over their hardware trough frameworks.

In this paper we present an implementation of a popular evolutionary inspired algorithm called CMA-ES.
The CMA-ES (Covariance Matrix Adaptation Evolution Strategy) is an evolutionary algorithm for non-linear or non-convex continuous optimization problems. It is considered as state-of-the-art in evolutionary computation and has been adopted as one of the standard tools for continuous optimization. It is based on the principle of biological evolution, where in each iteration new candidate solutions are generated. 

Choosing the framework was relatively straight forward. Most frameworks are not mature enough or proprietary, hence we chose openCl. OpenCL (Open Computing Language) is the first truly open and royalty-free programming standard for general-purpose computations on heterogeneous systems. It allows programmers to preserve their expensive source code investment and easily target multi-core CPUs, GPUs, and the new APUs. It improves the speed and responsiveness of a wide spectrum of applications. OpenCL is defined with four models, namely Platform model, Execution model, Memory model, and Programming model \cite{Gaster2012}. The most important beeing the execution model that deals with the loading and executing kernels on the GPU and Memory model, handling the allocation of GPU memory and the transfer of data between host memory and GPU memory.

The main challenge is to adapt the kernels and memory models depending on the hardware specifications namely, work-group sizes, compute units, etc. In this paper present empirical results comparing the standard algorithm with our GPGPU implementation. Additionally, we show results comparing two platforms from different hardware manufacturers to eliminate the potential bias of openCl implementation.

\section{Implementation}

The main loop in the CMA-ES algorithm consists of three main parts and it runs until termination criterion is met. In the first part the algorithm samples new population of solutions, for $k = 1, ..., \lambda$, in the second part it reorders the sampled solutions based on the fitness function and finally it updates the covariance matrix as described in \cite{Hansen2011}. 

The execution model defines how the OpenCL environment is configured on the host and how kernels are executed on the device. Before a host can request that a kernel is executed on a device, a context must be configured on the host that coordinates the host-device interaction and keeps track of the kernels that are created for each device. Each kernel contain a unit of code (function) which is executed on a specific compute unit. This units are then grouped into one, two or three dimensional set, which are called work-groups. 

For the implementation we obtained the source code of the iterative CMA-ES algorithm from the jMetal Java based framework for multi-objective optimization \cite{DNA10}. To speed-up the algorithm we integrated OpenCL into the execution process. In the first part the algorithm selects the OpenCL device with the largest number of compute units in addiction to the work group size.

After that it initializes the OpenCL context with the OpenCL $clCreateContext$ command. The OpenCL context is required for managing OpenCL objects such as command-queues, memory, program and kernel objects and for executing kernels on one or more devices specified in the context. It continues with creating the OpenCL command-queue with the command $clCreateCommandQueue$. 

The main loop of the CMA-ES algorithm is divided into kernels which are enqueued to the command-queue with the OpenCL $clEnqueueNDRangeKernel$ command. This kernels are than executed on the device. When the termination criterion is met, the host machine reads the best solution from the device memory with the OpenCL command $clEnqueueReadBuffer$ and outputs the result.

\begin{algorithm}
	\caption{CMA-ES (Sequential version)}\label{CMAES_SEQ}
	\begin{algorithmic}[1]
		\Procedure{execute()}{}
		
		\State {...}
		\State {// initialize variables}
		\State {init()}
		\State {...}
		
		\While{$counteval < maxEvaluations$}
			\State {// get a new population of solutions}
			\State {population = \textbf{samplePopulation()}}
			
			\For {$i \in 0...populationSize$}
				\State {// evaluate solution}
				\State {evaluateSolution(population.get(i));}
				\State {// update counteval}
				\State {counteval += populationSize;}
			\EndFor
			
			\State {// returns the best solution using a Comparator}
			\State {\textbf{storeBest(comparator);}}
			
			\State {// update the covariance matrix}
			\State {\textbf{updateDistribution();}}
		\EndWhile
		
		\State {...}
		
		\State {// store the best solution }
		\State {resultPopulation.add(bestSolutionEver);}
		
		\State {...}
		
		\State {// return the best solution}
		\State {return resultPopulation;}
		
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

The analysis of the iterative CMA-ES algorithm has shown that the most time expensive steps are in the \textit{SamplePopulation} method, in the inner for loop and in the \textit{UpdateDistribution} method. To speed-up the algorithm we implemented eight OpenCL kernels, which encapsulate the logic of those methods. In order to execute this kernels on the device, the implementation firstly initializes these kernels and their input arguments with the OpenCL $clSetKernelArg$ command. After that it sends all the precomputed data from the host to the device with the $clCreateBuffer$ command. All this actions are implemented in the \textit{init()} method. 

After the initialization process has finished the main loop of the algorith is executed as shown in algorithm \ref{CMAES_CL}. In each iteration of the main loop the \textit{SamplePopulation} method executes the OpenCL kernels with the OpenCL command  $clEnqueueNDRangeKernel$. When the \textit{SamplePopulation} method has finished the OpenCL device holds the current best solution found according to the fitness function which is implemented in the $computeFitnessKernel$. 

In the last phase of the main loop the algorithm executes all the kernels in the \textit{UpdateDistribution} method, which updates the covariance matrix as described in \cite{Hansen2011}. When the number of evaluations is bigger than the predefined maximum evaluation number, the host machine reads the best solution from the device memory, with the OpenCL command $clEnqueueReadBuffer$ and outputs the result as shown in algorithm \ref{CMAES_CL}.

\begin{algorithm}
	\caption{CMA-ES (OpenCL version)}\label{CMAES_CL}
	\begin{algorithmic}[1]
		\Procedure{execute()}{}
		
		\State {...}
		\State {// Host: initialize variables}
		\State {// Host: initializes kernels and input arguments}
		\State {// Host: send data to OpenCL device}
		\State {init()}
		\State {...}
		
		\State {// Host: main loop}
		\While{$counteval < maxEvaluations$}
			\State {// Host: execute kernels to sample population}
			\State {\textbf{samplePopulation();}}
			
			\State {// Host: update counteval}
			\State {counteval += (populationSize*populationSize);}
			
			\State {// Host: execute kernels to update the covariance matrix}
			\State {\textbf{updateDistribution();}}
		\EndWhile
		
		\State {...}
		
		\State {// Host: read the best solution from the device}
		\State {\textbf{clEnqueueReadBuffer(...);}}
		
		\State {...}
		
		\State {// Host: store the best solution }
		\State {resultPopulation.add(bestSolutionEver);}
		
		\State {...}
		
		\State {// Host: return the best solution}
		\State {return resultPopulation;}
		
		\EndProcedure
	\end{algorithmic}
\end{algorithm}

\section{Results}
The tests were performed on two main hardware configurations. Besides testing the performance improvement of our GPU implementation, we tested the differences between Nvidia and ATI graphic cards. 
The first machine was equipped with an i7-3820 CPU clocked at  3.60 GHz coupled with a GeForce GTX 650.
The second configuration was an AMD FX-6300 clocked at 3500 MHz with a AMD Radeon R9 280X.
The details of both cards are shown in Table \ref{tab:hardware}

\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | c |}
\hline
Property & GeForce GTX 650 & AMD Radeon R9 280X \\
\hline
Shading Units & 384 & 2048 \\
\hline
Memmory & 1024 MB & 3072 MB \\
\hline
GPU Clock & 1058 MHz & 850 MHz \\
\hline
Memory Clock & 1250 MHz & 1500 MHz \\
\hline
\end{tabular}
\caption{GPU hardware comparison}
\label{tab:hardware}
\end{center}
\end{table}
		
The jMetal framework offers plentiful problems to test optimization algorithms. We have chosen four typical problems, namely Rosenbrock, Sphere, Griewank, and Rastrigin. Each problem was tested on each hardware configuration and with different sample sizes. We measured the running time of the algorithm for each run and computed averages for each sample size. The results for tests ran on GeForce GTX 650 are shown in Table \ref{tab:nvidia_results}

\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | c | c | c |}
\hline
Sample size & Griewank   & Sphere & Rosenbrock & Rastrigin \\
\hline
10 & 6ms & 6ms & 6ms & 7ms\\
\hline
100 & 9ms & 9ms & 9ms & 9ms \\
\hline
500 & 79ms & 73ms & 73ms & 72ms \\
\hline
1000 & 226ms & 232ms & 231ms & 225ms \\
\hline
2500 & 1274ms & 1272ms & 1278ms & 1271ms \\
\hline
5000 & 5260ms & 5273ms & 5270ms & 5275ms \\
\hline
\end{tabular}
\caption{Run times on Nvidia GeForce 650gtx}
\label{tab:nvidia_results}
\end{center}
\end{table}

To illustrate the performance improvement we ran the same tests on the Intel i7-3820 CPU. The results are shown in Table \ref*{tab:intel_results}

\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | c | c | c |}
\hline
Sample size & Griewank   & Sphere & Rosenbrock & Rastrigin \\
\hline
10 & 11ms & 11ms & 11ms & 12ms\\
\hline
100 & 108ms & 112ms & 110ms & 113ms \\
\hline
500 & 856ms & 881ms & 879ms & 886ms \\
\hline
1000 & 5428ms & 5533ms & 5588ms & 5610ms \\
\hline
2500 & 123.39s & 121.57s & 153.94s & 127.89s \\
\hline
5000 & 1208.84s & 1184.37s & 1188.17s & 1265.53s \\
\hline
\end{tabular}
\caption{Run times on Intel i7-3820 CPU}
\label{tab:intel_results}
\end{center}
\end{table}

Comparing Tables \ref{tab:nvidia_results} and \ref{tab:intel_results} we can observe the improvements of the GPU implementation over the CPU. On larger problems, the GPU implementation outperforms the CPU by a factor of 230. On smaller tests however, the improvements are not substantial. This was expected due to the fact the kernels and the data need to be loaded from RAM to VRAM. In smaller problems the latency gives expression, while in the case of larger problems it does not impact overall computation time as much. The performance increase can also be observed in Figure \ref*{fig:nvidia_vs_intel} which was plotted in logarithmic scale.

\begin{figure}[h]
\centering
\includegraphics[width=1\linewidth]{./figures/nvidia_vs_intel}
\caption{Runtime comparison on logarithmic scale}
\label{fig:nvidia_vs_intel}
\end{figure}

We perform the same tests on the second configuration using an AMD-FX-6300 and an ATI Radeon R9-280x GPU shown in Tables \ref{tab:ati_results}
The results on the ATI GPU are a bit surprising. The graphic card is generally one of the fastest currently on the market, yet, it seems the Nvidia outperformed it on smaller problems. After profiling the execution using Jprofiler \cite{shirazi2002tool} we noticed the bottleneck was the AMD CPU, which took much longer to load the kernels and data on to the GPU. On larger problems, where the actual computation takes longer, the slow bandwidth between CPU and GPU is not noticeable.

\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | c | c | c |}
\hline
Sample size & Griewank   & Sphere & Rosenbrock & Rastrigin \\
\hline
10 & 31ms & 26ms & 28ms & 25ms\\
\hline
100 & 32ms & 31ms & 37ms & 30ms \\
\hline
500 & 86ms & 85ms & 92ms & 94ms \\
\hline
1000 & 219ms & 216ms & 232ms & 221ms \\
\hline
2500 & 1090ms & 1092ms & 1102ms & 1085ms \\
\hline
5000 & 4119ms & 4125ms & 4140ms & 4134ms \\
\hline
\end{tabular}
\caption{Run times on ATI Radeon R9-280x}
\label{tab:ati_results}
\end{center}
\end{table}

From Table \ref{tab:amd_results} we can observe the performance of the CPU implementation on the AMD CPU. Compared with the Intel CPU we can clearly see that the AMD CPU is much slower, which in turn explains the bottleneck in bandwidth between the GPU. The performance improvement of the GPU over CPU on the AMD configuration is shown in Figure \ref{fig:radeon_vs_amd} plotted in logarithmic scale.

\begin{table}[h]
\begin{center}
\begin{tabular}{| c | c | c | c | c |}
\hline
Sample size & Griewank   & Sphere & Rosenbrock & Rastrigin \\
\hline
10 & 17ms & 17ms & 17ms & 17ms\\
\hline
100 & 152ms & 130ms & 140ms &  139ms \\
\hline
500 & 1864ms & 1824ms & 1803ms & 1871ms \\
\hline
1000 & 24128ms & 24028ms & 24106ms &  24191ms\\
\hline
2500 & 232.52s & 229.29s & 223.91 & 234.76s \\
\hline
5000 & 3416.5s & 3446s & 3431.2s & 3445.2s \\
\hline
\end{tabular}
\caption{Run times on AMD FX-6300}
\label{tab:amd_results}
\end{center}
\end{table}

\begin{figure}[h]
\centering
\includegraphics[width=1\linewidth]{./figures/radeon_vs_amd}
\caption{Runtime comparison on logarithmic scale}
\label{fig:radeon_vs_amd}
\end{figure}

\section{Conclusion}
Using an open source meta-heuristic algorithm framework we implemented a GPGPU version of CMA-ES algorithm using openCL, which is also open source.
We empirically compared the computational results between the CPU and GPU version, which show improvement of the GPU over the CPU version. Additionally we compared two different configurations in an attempt to eliminate the possible bias of the framework towards certain manufacturers. Even though the configurations were not in the same price range and hence not directly comparable we show that the speedups obtained were in a reasonable margin. In best case our implementation is over 800 times faster then the CPU implementation. We did not notice any improvement in case of smaller problems mainly due the time penalty of transferring problem data from RAM to vRAM.

\section{Acknowledgment}
The authors would like to thank dr. Peter Korošec for advising and guiding the research.

\section{Future Work}
The hardware used to perform tests was not directly comparable. The Intel configuration had a much better CPU while the AMD configuration had a state of the art GPU. The next step is to run the tests on hardware configurations of the same price point. Additionally we are working on upstreaming our implementation to the Jmetal framework.

\balance
\bibliographystyle{abbrv}
\bibliography{bibliography}

\end{document}


